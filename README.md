## WPezToolbox - Loader 

__The engine that powers WPezToolbox. Leans heavily on TGM Plugin Activation by @thomasgriffin, @GaryJones and @jrfnl. (See link below).__

A plugin for loading plugins. 

Use it to organize your toolbox of WordPress plugins. 

Use it to publish and share bundles with others. 

> --
>
> Special thanks to JetBrains (https://www.jetbrains.com/) and PhpStorm (https://www.jetbrains.com/phpstorm/) for their support of OSS and its devotees. 
>
> --

### Important

The plugins contained in WPezToolbox bundles are simply "you might consider these suggestions." They are not formal endorsements; there is not claim being made about their quality and/or functionality. Also, some links might be affiliate links. 


### Overview

Yes! That's right!! A plugin for organizing and loading other plugins. 

Aside from organizing, bundles are an advantage because:

1) Bundles provide extra links related to the plugins. That is, you can shop before you buy (i.e., install). 

2) The plugins in the bundle aren't actually installed, until you choose to do so. In a way, a bundle is a wish list for plugins.

3) Bundles can be nested. That is, for example, the Bundle: Default contains the Bundle: Dev bundle and that contains the Bundle: Dev Themes. 

4) You can easily make your own custom bundles. 

5) You can share your custom bundles with your team, your colleagues and/or your followers. Imagine writing a blog post "The Best WordPress Plugins For _______" and then providing a .zip (i.e., bundle) that makes it ez for people to install and test your recommendations. How cool - and ez - is that?

### The Bonus Round

WPezToolbox Loader can also access and install plugins from public repos (e.g., GitLab, GitHub, BitBucket, etc.). It's not limited to plugins on WP.org.


### Getting Started

Download the plugin's zip:

https://gitlab.com/wpezsuite/WPezToolbox/wpez-toolbox-loader/-/archive/master/wpez-toolbox-loader-master.zip

Install it, and activate it as you would any WordPress plugin. 

Under the (Admin) Plugins menu look for the sub-menu: WPezToolbox. The plugins in your bundles will be listed there. Install and/or activate them as you see fit. 



### FAQ

__1 - Why?__

Mainly, too many plugins in my toolbox, not enough brain capacity to remember them :)

They also change slight from one client/project/moment to the next and I needed a higher level of flexibility. For example, I don't always need Query Monitor and all the add-ons for that ecosystem.

Most importantly, after seeing the same sets of recommendations come up various Facebook groups, a way to collect and organize such things felt like it could be beneficial to others. Think of a given bundle as an answer to the ever popular question: "What's the best plugin for _____?" 


__2 - Why not use Composer?__

Great question! Two reasons:

Anyone who can install a plugin, can use this tool. Composer is a great, but it also requires a deeper commitment to a developer-centric set of skills and workflow.

Not every client, project and/or host is Composer-friendly. 

WPezToolbox isn't intended to replace Composer (obviously), but to create a Composer-esque experience (when Composer isn't an ideal fit).


__3 - Doesn't Server Press' DesktopServer's blueprints do this?__

Yes and no. Those are less portable. That is, they are exclusive to DesktopServer. Blueprints also requires you install all the plugins you want.

On the other hand, WPezToolbox bundles can be using with any instance of WordPress, anywhere, anytime. Also, aside from the bundles, the plugins aren't installed until you need them. Yet they are there, just a click away, waiting for you summons them. 


__4 - This doesn't follow the WP naming convention?__

Yup. Don't panic. That's okay. Relax. Everything is gonna be alright.


 __5 - I'm not a developer, can we hire you?__
 
 Yes, that's always a possibility. If I'm available, and there's a good mutual fit. 


### Helpful Links

- http://tgmpluginactivation.com/

- https://github.com/TGMPA/TGM-Plugin-Activation

- https://gitlab.com/wpezsuite/WPezToolbox

- https://gitlab.com/wpezsuite/WPezToolbox/wpez-toolbox-bundle-default

- https://getcomposer.org/

- https://serverpress.com/




### TODO 

- Rewrite TGMPA so multiple classes aren't in the same file.

- Explore the possibility of listing premium plugins but not being able to install them

- Boilerplate for bundles + document filters.



### CHANGE LOG

- v0.0.1 - 4 January 2020
   
   FIXED - Activation error message

- v0.0.0 - 25 November 2019
   
   General updates
   
- v0.0.0 - 18 November 2019
   
   README updates

- v0.0.0 - 11 November 2019
   
   Proof of Concept

