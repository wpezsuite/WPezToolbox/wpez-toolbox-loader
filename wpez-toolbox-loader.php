<?php
/*
Plugin Name: WPezToolbox - Loader (POC)
Plugin URI: https://gitlab.com/wpezsuite/wpeztoolbox/wpez-toolbox-loader
Description: Loads WPezToolbox bundles
Version: 0.0.1
Author: Mark "Chief Alchemist" Simchock for Alchemy United
Author URI: https://AlchemyUnited.com
License: GPLv2 or later
Text Domain: wpez_tbox
*/

namespace WPezToolboxLoader;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.0 403 Forbidden' );
	die();
}

if ( ! class_exists( 'TGM_Plugin_Activation' ) ) {

	require_once( 'inc/class-tgm-plugin-activation.php' );
}

register_activation_hook( __FILE__, __NAMESPACE__ . '\registerActivationHook' );

function registerActivationHook() {
	// TODO
}


function tgmpaConfig() {

	/**
	 * Array of configuration settings. Amend each line as needed.
	 *
	 * TGMPA will start providing localized text strings soon. If you already have translations of our standard
	 * strings available, please help us make TGMPA even better by giving us access to these translations or by
	 * sending in a pull-request with .po file(s) with the translations.
	 *
	 * Only uncomment the strings in the config array if you want to customize the strings.
	 */

	$arr_config_default = array(
		'id'           => 'wpez_toolbox',
		// Unique ID for hashing notices for multiple instances of TGMPA.
		'default_path' => '',
		// Default absolute path to bundled plugins.
		'menu'         => 'wpez-toolbox-install-plugins',
		// Menu slug.
		'parent_slug'  => 'plugins.php',
		// Parent menu slug.
		'capability'   => 'edit_theme_options',
		// Capability needed to view plugin install page, should be a capability associated with the parent menu used.
		'has_notices'  => false,
		// Show admin notices or not.
		'dismissable'  => true,
		// If false, a user cannot dismiss the nag message.
		'dismiss_msg'  => '',
		// If 'dismissable' is false, this message will be output at top of nag.
		'is_automatic' => false,
		// Automatically activate plugins after installation or not.
		'message'      => '',
		// Message to output right before the plugins table.
		'strings'      => array(
			'page_title' => __( 'WPezToolbox: Install Plugins', 'wpez_tbox' ),
			'menu_title' => __( 'WPezToolbox', 'wpez_tbox' ),
			'return'     => __( 'Return to WPezToolbox: Install Plugins', 'wpez_tbox' ),
		),

		/*
		'strings'      => array(
			'page_title'                      => __( 'Install Required Plugins', 'theme-slug' ),
			'menu_title'                      => __( 'Install Plugins', 'theme-slug' ),
			// translators: %s: plugin name.
			'installing'                      => __( 'Installing Plugin: %s', 'theme-slug' ),
			// translators: %s: plugin name.
			'updating'                        => __( 'Updating Plugin: %s', 'theme-slug' ),
			'oops'                            => __( 'Something went wrong with the plugin API.', 'theme-slug' ),
			'notice_can_install_required'     => _n_noop(
				// translators: 1: plugin name(s).
				'This theme requires the following plugin: %1$s.',
				'This theme requires the following plugins: %1$s.',
				'theme-slug'
			),
			'notice_can_install_recommended'  => _n_noop(
				// translators: 1: plugin name(s).
				'This theme recommends the following plugin: %1$s.',
				'This theme recommends the following plugins: %1$s.',
				'theme-slug'
			),
			'notice_ask_to_update'            => _n_noop(
				// translators: 1: plugin name(s).
				'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.',
				'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.',
				'theme-slug'
			),
			'notice_ask_to_update_maybe'      => _n_noop(
				// translators: 1: plugin name(s).
				'There is an update available for: %1$s.',
				'There are updates available for the following plugins: %1$s.',
				'theme-slug'
			),
			'notice_can_activate_required'    => _n_noop(
				// translators: 1: plugin name(s).
				'The following required plugin is currently inactive: %1$s.',
				'The following required plugins are currently inactive: %1$s.',
				'theme-slug'
			),
			'notice_can_activate_recommended' => _n_noop(
				// translators: 1: plugin name(s).
				'The following recommended plugin is currently inactive: %1$s.',
				'The following recommended plugins are currently inactive: %1$s.',
				'theme-slug'
			),
			'install_link'                    => _n_noop(
				'Begin installing plugin',
				'Begin installing plugins',
				'theme-slug'
			),
			'update_link' 					  => _n_noop(
				'Begin updating plugin',
				'Begin updating plugins',
				'theme-slug'
			),
			'activate_link'                   => _n_noop(
				'Begin activating plugin',
				'Begin activating plugins',
				'theme-slug'
			),
			'return'                          => __( 'Return to Required Plugins Installer', 'theme-slug' ),
			'plugin_activated'                => __( 'Plugin activated successfully.', 'theme-slug' ),
			'activated_successfully'          => __( 'The following plugin was activated successfully:', 'theme-slug' ),
			// translators: 1: plugin name.
			'plugin_already_active'           => __( 'No action taken. Plugin %1$s was already active.', 'theme-slug' ),
			// translators: 1: plugin name.
			'plugin_needs_higher_version'     => __( 'Plugin not activated. A higher version of %s is needed for this theme. Please update the plugin.', 'theme-slug' ),
			// translators: 1: dashboard link.
			'complete'                        => __( 'All plugins installed and activated successfully. %1$s', 'theme-slug' ),
			'dismiss'                         => __( 'Dismiss this notice', 'theme-slug' ),
			'notice_cannot_install_activate'  => __( 'There are one or more required or recommended plugins to install, update or activate.', 'theme-slug' ),
			'contact_admin'                   => __( 'Please contact the administrator of this site for help.', 'theme-slug' ),
			'nag_type'                        => '', // Determines admin notice type - can only be one of the typical WP notice classes, such as 'updated', 'update-nag', 'notice-warning', 'notice-info' or 'error'. Some of which may not work as expected in older WP versions.
		),
		*/
	);

	$arr_config_custom = apply_filters( 'WPezToolboxLoader\config', $arr_config_default );

	if ( is_array( $arr_config_custom ) ) {
		return array_merge( $arr_config_default, $arr_config_custom );
	}

	return $arr_config_default;
}


function pluginsDefaults() {

	$arr = [

		'wpez-toolbox-bundle-default' =>
			[
				'name'     => __( '000 | WPezToolbox - Bundle: Default', 'wpez_tbox' ),
				// The plugin name.
				'slug'     => 'wpez-toolbox-bundle-default',
				// The plugin slug (typically the folder name).
				'source'   => 'https://gitlab.com/wpezsuite/WPezToolbox/wpez-toolbox-default/-/archive/master/wpez-toolbox-default-master.zip',
				// The plugin source.
				'required' => false,
				// If false, the plugin is only 'recommended' instead of required.
				//  'external_url' => 'https://github.com/thomasgriffin/New-Media-Image-Uploader', // If set, overrides default API URL and points to an external URL.
				'wpez'     => [

					'info' => [
						'by'     => 'Mark "Chief Alchemist" Simchock for Alchemy United',
						'url_by' => 'https://AlchemyUnited.com',
						'desc'   => 'The Bundle: Default for WPezToolbox. Use it, or filter it out and add your own bundle(s).',
						'url_img' => 'https://assets.gitlab-static.net/uploads/-/system/project/avatar/15292865/wpeztoolbox.png?width=64',
					],

					'resources' => [
						'url_repo' => 'https://gitlab.com/wpezsuite/WPezToolbox/wpez-toolbox-default',
						'url_site' => 'https://gitlab.com/wpezsuite/WPezToolbox',
						'url_tw'   => 'https://twitter.com/WPezDeveloper'
					]
				]
			]
	];

	$arr_mod = apply_filters( 'WPezToolboxLoader\plugins', $arr );

	if ( ! is_array( $arr_mod ) ) {
		$arr_mod = [];
	}

	return array_values( $arr_mod );


}


add_action( 'tgmpa_register', __NAMESPACE__ . '\tgmpaRegister' );
function tgmpaRegister() {


	$arr_plugins = pluginsDefaults();
	$config      = tgmpaConfig();

	tgmpa( $arr_plugins, $config );
}

add_filter( 'tgmpa_table_columns', __NAMESPACE__ . '\tgmpaTableColumns' );

function tgmpaTableColumns( $arr_columns ) {

	$arr_cols_new = [
		'cb'        => '<input type="checkbox" />',
		'plugin'    => __( 'Plugin', 'wpez_tbox' ),
		'info'      => __( 'Info', 'wpez_tbox' ),
		'resources' => __( 'Resources', 'wpez_tbox' ),
		'type'      => __( 'Type / Source', 'wpez_tbox' ),
	];

	return $arr_cols_new;

}

add_filter( 'tgmpa_table_data_item', __NAMESPACE__ . '\tgmpaTableDataItem', 10, 2 );

function tgmpaTableDataItem( $arr_table_data, $arr_row_plugin ) {

	$arr_row_new = rowPluginMerge( $arr_row_plugin );

	// TODO - move into a method
	$str_img = '';
	if ( isset( $arr_row_new['wpez']['info']['url_img'] ) && is_string( $arr_row_new['wpez']['info']['url_img'] ) ) {

		// TODO - don't inline the style
		$str_img .= '<div style="width:100%; max-width: 60px; float:right; margin: 0 0 5px 5px">';
		$str_img .= '<img src="' . esc_url( $arr_row_new['wpez']['info']['url_img'] ) . '" style="max-height: 60px">';
		$str_img .= '</div>';

	}

	$arr_table_data['plugin'] .= $str_img;

	$arr_table_data['info'] = tableDataInfo( $arr_row_new );

	$arr_table_data['resources'] = tableDataResources( $arr_row_new );

	$arr_table_data['type'] = $arr_table_data['type'] . '<br>' . $arr_table_data['source'];


	return $arr_table_data;

}

function labels( $str_key = false ) {

	$arr = [
		'info'      => [
			'by' => 'By'
		],
		'resources' => [
			'url_wp_org'  => 'WP.org',
			'url_repo'    => 'Repo',
			'url_site'    => 'Website',
			'url_premium' => '- Premium',
			'url_fb'      => 'FB',
			'url_tw'      => 'TW',
			'url_docs'    => 'Docs',
			'url_help'    => 'Help'
		],
	];

	if ( isset( $arr[ $str_key ] ) ) {
		return $arr[ $str_key ];
	}

	return $arr;
}


function rowPluginDefaults( $str_key = false ) {

	$arr = [

		'active' => true,

		'info'      => [
			'active'  => true,
			'by'      => false,
			'by_alt'  => false,
			'url_by'  => false,
			'desc'    => false,
			'url_img' => false,
		],
		'resources' => [
			'active'      => true,
			'url_wp_org'  => false,
			'url_repo'    => false,
			'url_site'    => false,
			'url_premium' => false,
			'url_fb'      => false,
			'url_tw'      => false,
			'url_docs'    => false,
			'url_help'    => false
		],
	];

	if ( isset( $arr[ $str_key ] ) ) {
		return $arr[ $str_key ];
	}

	return $arr;

}


function rowPluginMerge( $arr_row_plugin = false ) {

	if ( ! is_array( $arr_row_plugin ) ) {
		return [];
	}

	$arr_row_plugin_defaults = rowPluginDefaults();;
	// if we don't have a wpez (array) then set some defaults
	if ( ! isset( $arr_row_plugin['wpez'] ) || ! is_array( $arr_row_plugin['wpez'] ) ) {
		$arr_row_plugin['wpez'] = $arr_row_plugin_defaults;
		// when in doubt, set active to false
		$arr_row_plugin['wpez']['info']['active']      = false;
		$arr_row_plugin['wpez']['resources']['active'] = false;
	} else {

		if ( ! isset( $arr_row_plugin['wpez']['info'] ) || ! is_array( $arr_row_plugin['wpez']['info'] ) ) {
			$arr_row_plugin['wpez']['info'] = $arr_row_plugin_defaults['info'];
			// when in doubt, set active to false
			$arr_row_plugin['wpez']['info']['active'] = false;
		} else {
			$arr_row_plugin['wpez']['info'] = array_merge( $arr_row_plugin_defaults['info'], $arr_row_plugin['wpez']['info'] );
		}

		if ( ! isset( $arr_row_plugin['wpez']['resources'] ) || ! is_array( $arr_row_plugin['wpez']['resources'] ) ) {
			$arr_row_plugin['wpez']['resources'] = $arr_row_plugin_defaults['resources'];
			// when in doubt, set active to false
			$arr_row_plugin['wpez']['resources']['active'] = false;
		} else {
			$arr_row_plugin['wpez']['resources'] = array_merge( $arr_row_plugin_defaults['resources'], $arr_row_plugin['wpez']['resources'] );
		}

	}

	return $arr_row_plugin;
}

function tableDataActive( $arr_row_new ) {

}

function tableDataInfo( $arr_row_new ) {

	$str_ret = '';
	if ( ! isset( $arr_row_new['wpez']['info'] ) || ! is_array( $arr_row_new['wpez']['info'] ) ) {
		$arr_row_new['wpez']['info'] = [];
	}
	$arr_info   = array_merge( rowPluginDefaults()['info'], $arr_row_new['wpez']['info'] );
	$arr_labels = labels();

	if ( $arr_info['active'] === false ) {
		return '';
	}

	if ( is_string( $arr_info['desc'] ) && ! empty( esc_attr( $arr_info['desc'] ) ) ) {
		$str_ret .= esc_attr( $arr_info['desc'] ) . '<br><br>';
	}

	$str_open  = '';
	$str_close = '';
	if ( is_string( $arr_info['url_by'] ) && ! empty( esc_url( $arr_info['url_by'] ) ) ) {
		$str_open  = '<a href="' . esc_url( $arr_info['url_by'] ) . '">';
		$str_close = '</a>';
	}

	$str_by = false;
	if ( is_string( $arr_info['by'] ) && ! empty( esc_attr( $arr_info['by'] ) ) ) {
		$str_by = $arr_info['by'];
	}
	$str_link = '';
	if ( is_string( $arr_info['by_alt'] ) && ! empty( esc_attr( $arr_info['by_alt'] ) ) ) {

		$str_link = $str_open . esc_attr( $arr_info['by_alt'] ) . $str_close;
		if ( $str_by !== false ) {
			$str_link = esc_attr( $arr_labels['info']['by'] ) . ' ' . esc_attr( $str_by ) . ' [' . $str_open . esc_attr( $arr_info['by_alt'] ) . $str_close . ']';
		}

	} elseif ( $str_by !== false ) {
		$str_link = esc_attr( $arr_labels['info']['by'] ) . ' ' . $str_open . esc_attr( $str_by ) . $str_close;
	}

	$str_ret .= $str_link;

	/*
	$str_img = '<div style="height: 100%; background: red; width:100%; max-width: 75px; float:left; margin: 0 5px 5px 0">';
	if ( isset($arr_info['img']) &&  is_string($arr_info['img'])){

		$str_img .= '<img src="' . esc_url($arr_info['img']). '" style="max-height: 75px">';

	}
	$str_img .= '</div>';
	*/

	return $str_ret;

}

function tableDataResources( $arr_row_new ) {

	$arr_labels    = labels()['resources'];
	$arr_resources = [];

	if ( $arr_row_new['wpez']['resources']['active'] === false ) {
		return '';
	}

	foreach ( $arr_labels as $key => $label ) {

		$str_open  = '';
		$str_close = '';
		if ( isset( $arr_row_new['wpez']['resources'][ $key ] ) && is_string( $arr_row_new['wpez']['resources'][ $key ] ) ) {

			$str_open  = '<a href="' . esc_url( $arr_row_new['wpez']['resources'][ $key ] ) . '">';
			$str_close = '</a>';
			//	$arr_resources[] = $arr_labels[$key] . ' = ' . $arr_row_new['wpez']['resources'][$key];

		}

		$arr_resources[] = $str_open . esc_attr( $arr_labels[ $key ] ) . $str_close;

	}

	return implode( '<br>', $arr_resources );

}



